module.exports = {
  'customRoot': false,
  'routesToPrerender': [
    '/',
    '/gray',
    '/blue'
  ]
}
