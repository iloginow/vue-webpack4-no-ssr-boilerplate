export default {
  count: state => {
    return state.count
  },
  color: state => {
    return state.route.name.toLowerCase()
  },
  pageData: state => {
    return state.data.main
  },
  blueData: state => {
    return state.data.pages.blue
  },
  grayData: state => {
    return state.data.pages.gray
  },
  whiteData: state => {
    return state.data.pages.white
  }
}
