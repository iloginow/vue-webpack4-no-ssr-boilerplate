const White = () => import('../components/White')
const Blue = () => import('../components/Blue')
const Gray = () => import('../components/Gray')

export default {
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'White',
      component: White,
      meta: {
        title: 'Vue Template | White',
        description: 'Welcome to the Vue.js deno. This is a white page.'
      }
    },
    {
      path: '/gray',
      name: 'Gray',
      component: Gray,
      meta: {
        title: 'Vue Template | Gray',
        description: 'Welcome to the Vue.js deno. This is a gray page.'
      }
    },
    {
      path: '/blue',
      name: 'Blue',
      component: Blue,
      meta: {
        title: 'Vue Template | Blue',
        description: 'Welcome to the Vue.js deno. This is a blue page.'
      }
    }
  ]
}
